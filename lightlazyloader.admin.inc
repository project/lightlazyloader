<?php

/**
 * @file
 * LightLazyLoader admin form.
 */

/**
 * Callback for admin page of Lightlazyloader.
 */
function lightlazyloader_admin_settings() {
  $form = array();
  $form['lightlazyloader_enabled'] = array(
    '#type' => 'checkbox',
    '#title' => t('Enabled'),
    '#default_value' => variable_get('lightlazyloader_enabled', LIGHTLAZYLOADER_DISABLED),
    '#description' => t('Enable/Disable LightLazyLoader'),
  );

  $form['lightlazyloader_enable_specific_page'] = array(
    '#type' => 'fieldset',
    '#title' => t('Enable specific pages'),
    '#collapsible' => FALSE,
    '#collapsed' => TRUE,
  );

  $form['lightlazyloader_enable_specific_page']['lightlazyloader_exclude_option'] = array(
    '#type'          => 'radios',
    '#title_display' => 'invisible',
    '#default_value' => variable_get('lightlazyloader_exclude_option', LIGHTLAZYLOADER_EXCLUDE_OPTION),
    '#options'       => array(
      'exclude' => t('All pages except those listed'),
      'include' => t('Only the listed pages'),
    ),
  );

  $form['lightlazyloader_enable_specific_page']['lightlazyloader_paths'] = array(
    '#type' => 'textarea',
    '#title_display' => 'invisible',
    '#title' => t('Pages'),
    '#default_value' => variable_get('lightlazyloader_paths', LIGHTLAZYLOADER_PATHS),
    '#description'   => t("Specify pages by using their paths. Enter one path per line. The '*' character is a wildcard. Example paths are blog for the blog page and blog/* for every personal blog. <front> is the front page."),
  );

  return system_settings_form($form);
}
